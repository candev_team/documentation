@echo off

REM
REM Pandoc command for generating a PDF file.
REM
REM Pandoc and wkhtmltopdf must be installed.
REM

@echo on

set output_filename=CANdev_Getting_Started.html
if not [%1]==[] set output_filename=%1

pandoc CANdev_Getting_Started.md --to html5^
 --table-of-contents --standalone^
 --css ../style/documentation_style.css^
 --css ../style/numbered_sections.css^
 --variable title:"CANdev Getting Started Instructions"^
 --metadata pagetitle="CANdev Getting Started Instructions"^
 --self-contained^
 --output %output_filename%

echo Document generated.
