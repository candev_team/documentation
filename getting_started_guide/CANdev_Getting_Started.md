<!-- 

	NOTE: The title must be removed when generating PDF document!
	Otherwise it will be included in the table of contents.

Kvaser CANdev Getting Started Guide
===================================

-->


---

This file contains a description of the Kvaser Workbench CANdev software, including instructions for installing and using it.

---

About the CANdev Platform
-------------------------

The Kvaser CANdev platform is a software and hardware suite which aims to make it easy and accessible to develop embedded CAN systems.

CANdev is based on **CAN Kingdom**, a framework and a methodology for designing and managing CAN systems. To learn about CAN Kingdom, see the CAN Kingdom specification in the documentation folder of the release package, or the summary in [*CAN Kingdom*](#can-kingdom).

This release package contains the *CANdev platform* software. Its main component is *Workbench CANdev*.

*Workbench CANdev* is an Eclipse IDE based platform for modelling and developing source code for embedded systems. It consists of the Kingdom Founder application and the rt-labs rt-collab toolbox.

The content of the release package is the following:

* **A Windows installer for Workbench CANdev**: See below for more information about the workbench.

* **CAN Kingdom Runtime**: An implementation of the essential parts of CAN Kingdom. This is an Eclipse project containing a C library which application projects can use to support CAN Kingdom.  

* **CANdev examples**: Example project for the workbench. It demonstrates how to write C code for embedded units and how to use the Kingdom Founder application to develop models.

* **CANdev Loader**: A program for loading software to embedded units using CAN. It is available as an Eclipse plug-in in Workbench CANdev, and also as a command-line program.

* **CANdev Bootloader**: Bootloader software that supports download of software over CAN. The bootloader is pre-installed on the CANdev CPU boards.

* **Kingdom Founder Stand-Alone**: A version of the Kingdom Founder application which can simply be unzipped and started, without any installation.

* **Hardware Schematics**: For the hardware of the CANdev embedded units.

* **Documentation**: For the various CANdev components.

The following diagram shows the content of the release package:

    candev-release-XXXX
    │
    ├── Workbench_CANdev-X.X.X.exe
    ├── CANdev_User_Guide.pdf
    │
    ├── software
    │   ├── can_kingdom
    │   │   ├── KingSrc
    │   │   └── MayorSrc
    │   │
    │   ├── kingdom_founder_stand_alone
    │   │
    │   ├── candev_bootloader
    │   │   ├── candev_bootloader.bin
    │   │   └── candev_bootloader_refman.pdf
    │   │
    │   └── candev_loader
    │       ├── candev_loader.exe
    │       └── candev_loader_refman.pdf
    │
    ├── documentation
    │   ├── CAN_Kingdom_Specification_rev_3_01.pdf
    │   ├── Kingdom_Founder_Reference_Manual.pdf
    │   ├── rt-collab_toolbox_refman.pdf
    │   ├── rt-kernel_refman.pdf
    │   │
    │   ├── hardware_schematics
    │   └── processor_manuals
    │
    ├── drivers
    │   ├── canlib.exe
    │   └── kvaser_drivers_setup.exe
    │
    └── examples
        ├── carcan
        └── carcan_2


For more information about CANdev, CAN Kingdom, Kingdom Founder and the Kvaser company contact Kvaser at the website [kvaser.com](https://www.kvaser.com).
 
For more information about rt-collab toolbox or the rt-labs company see [rt-labs.com](https://www.rt-labs.com).


### Workbench CANdev

Workbench CANdev is and Eclipse based development environment for embedded software using rt-kernel and CAN Kingdom.

It consists of the following components:

* *Eclipse IDE*, with C Development Tools (CDT)
* *rt-collab toolbox* from rt-labs. This is an Eclipse plug-in that helps the user to set up and work with projects that use rt-kernel platform for embedded units. 
* *Kingdom Founder*. This is an Eclipse plug-in for modelling CAN systems using the CAN Kingdom methodology.
* *CANdev Loader* plug-in. This is an Eclipse plug-in for loading application software over CAN from the workbench to embedded units.
* The *rt-kernel* real-time operating system, with support for the CANdev hardware. The OS is linked to the embedded unit executables when they are built.
* *Board support software (BSP)*, for the CANdev hardware. 

### CANdev Hardware

Apart from the CANdev workbench software package the CANdev platform also has a hardware platform. It consists of a CPU board with an STM32 CPU.

A CPU board must be docked to a carrier board.

The currently supported carrier boards are the Servo board and the IO board. The hardware schematics for the boards are included in the CANdev release package.

To connect to the boards from a PC, a suitable CAN connection device is needed, for example the [Kvaser Leaf Light][leaf]. 

<p align="center"><img id="leaf-image" src="images/leaf_light.jpg" alt="Leaf" style="height: auto; width: 30%"/></p>


### Board Support Packages (BSP:s)

The board support packages (BSP:s) is software that implements support for some particular type of hardware. Embedded application projects link to the BSP:s to setup and interact with the hardware.

Workbench CANdev is bundled with a number of BSP:s. They can be imported into the workspace when a new rt-kernel project is created, or using *File* > *Import* > *rt-kernel reference BSP*.

All the BSP:s contains code for setting up rt-kernel and the basic functionality of an STM32 processor.

The following are the most frequently used BSP:s:

* **IO BSP**: Contains functionality for controlling IO devices using GPIO.
* **Servo BSP**: Contains functionality for controlling a servo using pulse-width modulation (PWM). 
* **CPU BSP**: Contains only the base BSP functionality, without support for any extra hardware. Can be used to develop support for custom hardware.


<a name="can-kingdom"></a>

### CAN Kingdom

CAN Kingdom is a framework for modelling and configuring CAN networks. This section contains some basic information to get the user started. The CANdev system is developed to work together with CAN Kingdom.

More detailed information about CAN Kingdom can be found in the CAN Kingdom specification, which is available in the CANdev package.

The following is a list of CAN Kingdom terms that are good to know:

* **City:** In CAN Kingdom an embedded unit in a CAN network is called a **city**. A city has an 8 bit **city number** which is used to identify it within the network.
* **Module:** A module is a specific type of an embedded unit. This can be contrasted to a city, which is a concrete instance of a module in a network.
* **Capital:** The master unit of the CAN network.
* **Document:** A messages on the CAN bus. A document describes which data values that a message contains.
* **Folder:** A folder is an assignment of a CAN frame ID to a document. 
* **King:** The control component of the master unit in the network. The king controls the other units in the network be sending the King's document to them.
* **Major:** The control software of a city.
* **Kingdom Founder:** The designer of the CAN network. This is also the name of the modelling tool that can be used by the human Kingdom Founder.

There are two main CANdev components that are related to CAN Kingdom: the CAN Kingdom runtime and the Kingdom Founder application. They are described in the sections below.

The following image illustrates some common CAN Kingdom terms. 

![](images/can_kingdom_diagram.png)


#### CAN Kingdom Runtime

This is a software component that is used as a library by application for embedded systems. It contains functionality for sending and receiving special CAN Kingdom messages that are used to configure the network and the embedded units. These special messages are called the "King's Pages" and the "Mayor's Pages".

The following are some examples of the uses of these messages:

* instruct a unit to enter silent mode and stop sending any messages on the CAN bus 
* instruct a unit or to reboot and wait for new software
* instruct a unit to use a certain CAN ID for a certain message

The CAN Kingdom runtime is available as a library project in the CANdev package.

#### Kingdom Founder

Kingdom Founder is an Eclipse based application for modelling CAN Kingdom networks. It is integrated as a plug-in in Workbench CANdev, but can also be run as a stand-alone application.

Kingdom Founder consists of three main parts:

* A *Resource Editor*, which is used to create signal specifications.
* A *Module Editor*, which most importantly is used to specify which kinds of messages a module can send and receive.
* A *System Editor*, which is used to connect modules to a network.

The CANdev package contains a separate reference manual for Kingdom Founder which lists all its functionality. 

### rt-kernel

rt-kernel is a secure, small, efficient and reliable embedded real-time operating system that is developed by rt-labs. It is the OS that is used by default by the CANdev embedded units.

When a new project is created in Workbench CANdev it is automatically set up to include the rt-kernel header files and link against rt-kernel when built.

The following are some of the services that are provided by rt-kernel:

* Multitasking
* Timers
* Interrupt services
* Event logging and monitoring

The reference manual for rt-kernel is available in the CANdev release package. 

### Examples

Two example projects are included in the CANdev package. They contains the source code for the software of embedded systems.

* *Car CAN* is a simple and minimal remote control car. It is used in the guides in the documents. More information about it can be found in the section [*Try Out the Car CAN Example*](#car-can).

* *Car CAN 2* is an evolved version of the original Car CAN project. It is more complete and capable, but also more complex. It is developed by Hashem Hashem and André Idoffsson for their master's thesis project. For more information see their thesis which is included in the release package documentation.

<p align="center"><img id="model-car" src="images/car_model_photo.jpg" alt="Model car" style="height: auto; width: 60%"/></p>

---

Installation and Setup
----------------------

### Install Workbench CANdev

Follow these steps:

1. Run the Workbench_CANdev-XXX.exe program (located in the root of the CANdev package). This will launch the workbench installer.
2. Follow the instruction in the installer.

The workbench should now be installed on the system.

If the workbench will be used to write executable files to embedded units then CAN drivers also must be installed. Information about driver can be found in section [*Install CAN Drivers and Related Software*](#install-drivers).

### Start Workbench CANdev

Follow these steps:

1. Run Workbench CANdev launcher. This can be done either from the start menu or with the file `workbench\Workbench_CANdev.exe` in the installation directory.
2. A dialog asking for an Eclipse workspace will open. Accept the default or choose another suitable workspace location.

The Eclipse application window should now open.

<a name="install-drivers"></a>

### Install CAN Drivers and Related Software 

To be able to transfer software to embedded units using CAN the following software needs to be installed. (This is however not required to use the workbench and work with the Kingdom Founder component.)

* Kvaser Drivers for Windows
* Kvaser CANlib SDK

Then can be installed using the installer programs in the `drivers` folder in the CANdev package root. They can also be downloaded from the Kvaser home page at [www.kvaser.com/downloads](https://www.kvaser.com/downloads/).

---

User Guide
----------

First make sure the instructions in *Installation and Setup* has been completed and that the Workbench CANdev application is started.

### Try Out the C Programming Tools

This section describes how to create a simple "Hello World" example project and how to download that resulting executable file to an embedded unit.

> **Note**: This example does not use CAN Kingdom or Kingdom Founder. It demonstrates only how to create a minimal example project which runs on a single embedded unit. See [*Try Out the Car CAN Example*](#car-can) for information about that.

Create a C project for embedded systems:

1. Click *File* > *New* > *C Project*

2. Name the project "test_module", make sure that *rt-kernel Project*, *Application* and *rt-kernel-stm32* are selected.

![](images/new_c_project_screenshot.png)

3. Click *Next* to go to the BSP tab, click *Import*.

4. Name the BSP project "test_bsp", make sure *rt-kernel-stm32* is selected, select any BSP type.

![](images/import_bsp_screenshot.png)

5. Click *Finish*.

Two projects will be created:

* A BSP project, *test_bsp*, which contain the source code for interfacing with the CANdev hardware.

* An application project, *test_module*, where the user can implement the source code of their embedded application.

Both projects will include and link to the rt-kernel source code.

The application project contains a main file, `src/main.c`. In the next step an executable file will be built from this source file.

6. Ensure that the *Output Type* for the application project is set to *Binary*: Right-click on the project > *Properties* > *C/C++ Build* > *Settings* > *GNU Objcopy* > *General*.

7. Right-click on the application project and choose *Build Project*.

A build information dialog will open. A *Binaries* entry containing a *test_project.elf* file will appear in the project.

The *Console* view contains the build status output.

![](images/build_finished_shreenshot.png)


<a name="write-exec"></a>

### How to Download Software Using CAN

When an application project has been built its executable file is ready to be downloaded to an embedded unit. This section describes how to do this over CAN using the *CANdev Firmware Download* tool.

1. Connect the embedded unit to the PC using a suitable CAN connection device.

2. Right-click on the project, select *Run As* > *CANdev Firmware Download*.

    The first time this is done a new external tool is created and the *External Tools Configurations* dialog will open. In the future this will run the existing tool.

![](images/firmware_download_menu.png)

3. In the dialog, set *Executable File* to the `.bin` file located in the `Debug` sub-folder of the project using the *Browse...* button.

4. Clear *City Number*, *Configuration File* and *Folders File* so that they are both empty.

    These fields are not used in this simple Hello world example. Information about these fields can be found in section [*How to Download Software to a CAN Kingdom Unit*](#can-kingdom-download).

5. The *Channel* field often can be left set to 0, but this value depends on how the CAN connection device is configured. The *Bitrate* field should be left set to 125 Kbit.

6. Press *Run* to download the firmware.

7. Immediately after pressing *Run*, power-cycle the embedded unit.

The download process should start. 

The *CANdev Firmware Download* tool works by sending "download request" messages on the CAN bus with a high frequency. The embedded unit bootloader will check for these messages during a short period on start-up. If a download message is received by the unit it will pause start-up, download the new software, and then run the new executable.

![](images/simple_firmware_download.png)

Executable files can also be downloaded using a command line tool. See the CANdev Loader manual for details about how to do this.

> **Note**: Some trouble-shooting tips for firmware download can be found in [*How to Download Software to a CAN Kingdom Unit*](#can-kingdom-download).


<a name="car-can"></a>

### Try Out the Car CAN Example

The Car CAN projects is a demo application for the CANdev system. Its purpose is to demonstrate the most important concept in CANdev: The use of CAN Kingdom, the CANdev Workbench, rt-kernel, BSP:s and application projects.

Prerequisites:

1. Install CANdev Workbench.

2. Open the CANdev Workbench.

3. Import the Car CAN example projects into the workspace: *File > Import... > Existing Projects into Workspace*, select `<CAN_DEV_ROOT>/examples/carcan`, click *Finish*.

4. Build the projects: *Project > Build All*. The build progress can be examined in a *Console* window. Verify that the build completes without problems.

#### Car CAN Description

The Car CAN system consists of three embedded units: two servo units and one joystick unit. The servo units have identical software. Messages are sent over a CAN bus from the joystick to control the servos. The Kvaser [Air Bridge](https://www.kvaser.com/airbridge/) device is used to send wireless CAN data from the joystick to the servos.  

Car CAN consists of 6 Eclipse projects:

![](images/car_can_projects.png)

##### Application Projects

There are two application projects: `carcan_joystick` and `carcan_servo`.

- They contain source code that implements the logic of how a joystick and a servo works.
- They use the other projects as libraries.
- The joystick gets input from the joystick hardware, performs some computation on that, then sends can messages with this data.
- The servo receives those messages, performs some computation, then outputs control signals to the servo hardware.
- These projects contain some template source code that is generated by Kingdom Founder and then manually copied into the application projects: `ckJoystick.h`, `ckJoystick_dispatch.c` and `ckJoystick_handlers.c` (and similar files for the servo).
- When these projects are built they produce the final executable files that are run on the embedded units: `/carcan_joystick/Debug/carcan_joystick.bin` and `/carcan_servo/Debug/carcan_servo.bin`.

##### BSP Projects

There are two BSP projects: `bsp_joystick` and `bsp_servo`

- The BSP projects contains C source code for starting, settings up and controlling the hardware for one particular card.
- They are used as library projects by the application projects: `bsp_joystick` is used by `carcan_joystick` and `bsp_servo` is used by `carcan_servo`.
-  `bsp_joystick` contains code for controlling hardware using GPIO.
-  `bsp_servo` contains code for controlling hardware using a PWM (pulse-width modulation) chip.

##### CAN Kingdom Project

The `can_kingdom` project contains the CAN Kingdom runtime implementation.

- It contains C source code that implements the CAN Kingdom framework runtime.
- It is used as a library project by the application projects.
- More information can be found in the section about [*CAN Kingdom*](#can-kingdom).

##### Kingdom Founder Model

The Kingdom Founder model of the system is in the `_car_model` project.

Its main components are these:

* The module files, `ckJoystick.kfm` and `ckServo.kfm`.

    These files specify the properties of the joystick and the servos. Most importantly, they specify which messages that the units can send and received.

* The system specification file, `system.kfs`.  

    It specifies how the modules are used in the system, the ID:s that each unit uses and what messages units send to each other. The system file references the two module files.

* The resource files, `.kfr`. 

    These files specify a set of data components that the modules files in their turn use to specify the contents of their messages.

The model is used for the following purposes:

* As documentation. It is easier for humans to read a system specification than to read source code.
* For validation. The Kingdom Founder application validates some properties of the design, such that senders and receivers   of a message are using the same data format.
* For source code and configuration file generation. The Kingdom Founder application can generate basic boiler plate source code: C structs for messages types, constant declarations for messages and unit ID:s, etc.

![](images/carcan_model_project.png)

#### Module Models

* A module is a specific type of an embedded unit. A system can contain multiple cities (embedded units) that all use the same module. The most important information that a module contains is the specification of which messages a it can send and receive.

* Double-click on one of the module files (`ckJoystick.kfm` or `ckServo.kfm`) to open the *Module Editor*.

* The *Documents* tab in the editor contains the list of messages that the module can send or receive.

* The Joystick module for example has documents that specify that it can send its X and Y values on the CAN bus. The Servo module has documents that specify that it can receive those messages.

* Each document consists of a list of the signals that make out the data content of the message.

![](images/joystick_documents.png)

#### System Model

* The *System Editor* is used to connect cities into to a network. This is called a "kingdom" in CAN Kingdom.
* Double-click on the system file, `system.kfs` to open the Car CAN system model.
* The system consists of three embedded units, which are called or "cities" in CAN Kingdom.
* Double-click on one of the cities to navigate to its entry in the *Properties* tab.

![](images/car_can_system.png)

* The *Properties* tab contains some general information about the system, and a list of all the cities in the system.
* The most important information about each city is this:
    - Its module. This field references one of the modules files, and specifies which messages the city can send and receive.
    - The *City Number*. This is the ID of the city. An embedded unit is given its city number by downloading a configuration file using the *CANdev Firmware Download* tool. The configuration file is generated from the value in this field. More information about this is given below.

![](images/car_can_system_properties.png)

* The *Connections* tab specifies which messages each city actually sends and to which cities it sends them.
* One of the features of CAN Kingdom is that CAN messages ID:s (that is, the arbitration fields) are not hard-coded in the embedded units. The ID:s are instead specified by the Kingdom Founder (the system designer) using the "folders" lists of the sender and receiver of the messages.
* Each entry in the connections list specifies one folder for the producer city (the sender) and one folder for each consumer cities (the receivers).
* The information about folders can be transmitted to the cities in one of two different ways:
    - By downloading a folder configuration file using the *CANdev Firmware Download* tool. The folder configuration file is generated from the data this editor tab. More information about this is given below.
    - By using a master embedded unit, called a "king" in CAN Kingdom. This unit sets up the folders of the other units by sending configuration messages during start-up.
* When a message is received by a city the CAN ID of the message is compared to the unit's list of configured folders. If a match is found the unit interprets the message as an instance of the relevant document.

![](images/car_can_system_connections.png)

* *System-Level Documents* is an advanced topic which is not covered by this guide.

#### Generated Files

The Kingdom Founder application can be used to generate C source code and configuration files that are used by the cities. This is done from the editor menu in the system editor.

![](images/generate_button.png)

Generated files from the Car CAN model are already present in the folder `_car_model/generated_code`. They can be re-generated by opening the file `system.kfs` and using the code generation button.

The following are the generated files:

* **CAN Kingdom King source code**: `system.c` and `system.h`. These files contain a basic CAN Kingdom King. The code sends King's page messages for setting up the folders in the servo and joystick cities in the system. This code is not used in the Car CAN example since a King is not used, instead folder configuration files are downloaded with the *CANdev Firmware Download* tool.

* **Module source code**: This code is located in the `modules` sub-directory. It contains basic template source code for the joystick and servo applications. This includes:
    - C struct declarations for the contents of all messages.
    - Functions for sending are receiving the messages.
    - A basic application skeleton with a main loop and callbacks from the CAN Kingdom runtime. 

* **City configuration files**: These files are located in the `cities` sub-directory. 
    - The file `ck.cfg` contain the city number and base number. 
    - The file `folders.cfg` contains the specification of the CAN Kingdom folders that the unit will use. 
    - The folders specify which CAN ID:s that will be used for the messages that the unit sends and received.
    - The files above must be downloaded to the embedded units for the system to work. That is done with the *CANdev Firmware Download* dialog.
    - The `.ini` files contains the same information as the `.cfg` files, but in a human readable format. The are only used for debugging purposes.


<a name="can-kingdom-download"></a>

### How to Download Software to a CAN Kingdom Unit

This section describes how to download an executable file and configuration files to a CAN Kingdom embedded unit. To do this the *CANdev Firmware Download* tool is used.

The Servo unit in Car CAN is used as an example.

1. Connect the embedded unit to the PC using a suitable CAN connection device.

2. Right-click on the project `carcan_servo`, select *Run As* > *CANdev Firmware Download*.

    The first time this is done a new external tool is created and the *External Tools Configurations* dialog will open. In the future this will run the existing tool.

![](images/firmware_download_menu.png)

3. In the dialog, set *Executable File* to `carcan_servo/Debug/carcan_servo.bin` using the *Browse...* button.

    This is the executable program file of the application which will be run on the embedded unit.

4. Leave *City Number* empty. 

    This is an optional field which is used to designate a CAN Kingdom unit as the receiver. This field is used if there are multiple units the CAN bus. For this to work the units on the CAN bus must already have received a city number. Leave this field empty when programming the Car CAN servo for the first time.

5. Set *Configuration File* to `model/generated_code/cities/servo/ck.cfg` using the *Browse...* button.

    This is an optional field which contains a path to a configuration file. When written to a unit this file assigns a city number. To do this the first time, only a single unit must be present on the CAN bus, otherwise multiple units might get assigned the same number, leading to no end of confusion. This file can be generated by Kingdom Founder.

6. Set *Folders File* to `model/generated_code/cities/servo/folders.cfg` using the *Browse...* button.

    This is an optional field which contains a file which when written to a unit assigns CAN ID:s to its difference messages. This file can be generated by Kingdom Founder. 
    
7. The *Channel* field often can be left set to 0, but this value depends on how the CAN connection device is configured. The *Bitrate* field should be left set to 125 Kbit.

8. Press *Run* to download the firmware.

9. Immediately after pressing *Run*, power-cycle the embedded unit.

The download process should start.

![](images/can_kingdom_firmware_download.png)

The *CANdev Firmware Download* tool works by sending "download request" messages on the CAN bus with a high frequency. The embedded unit bootloader will check for these messages during a short period on start-up. If a download message is received by the unit it will pause start-up, download the new software, and then run the new executable.

> **Note**: Once a download configuration has been created it can be accessed from the toolbar. 
> <p align="center"><img id="external-tools-toolbar" src="images/external_tools_toolbar_button.png" alt="J-Link" style="height: auto; width: 40%"/></p>

> **Note**: If an error message appears when creating *CANdev Firmware Download* launch it is most likely caused by missing CAN drivers. To solve this, install the CAN drivers as described in section [*Install CAN Drivers and Related Software*](#install-drivers).

> **Note**: If an error message appears after that the *Run* button has been pressed it can be tricky to figure out the problem.
>
> It might be caused by one of the following problems:
>
> - A faulty CAN bus, for example with a faulty or missing terminal resistor(s).
> - An embedded unit which does not have a bootloader installed.
> - A misconfigured CAN connection device, for example using the wrong channel or bitrate.
> - Faulty data in the *CANdev Firmware Download* dialog, for example references to non-existent files.
>
> <p align="center"><img id="external-tools-toolbar" src="images/candev_loader_error_message.png" alt="CANdev Fireware Download Error" style="height: auto; width: 70%"/></p>


<a name="connect-debugger"></a>

### How to Connect a J-Link Debugger

A debugger device can be used to connect to an embedded unit. The follow are the most basic uses of a debugger: 

* Stop a program on a break point and examine its internal data.
* Download executable files.

This section describes how to connect a Segger J-Link debugger device to a CANdev unit, using the GDB feature that is available in Workbench CANdev. A J-Link debugger device can be purchased from the Segger web site.

<p align="center"><img id="jlink-image" src="images/jlink_debugger.png" alt="J-Link" style="height: auto; width: 20%"/></p>

Start J-Link GDB Server:

1. Download and install the J-Link software. It is available from the Segger web site.
2. Connect the debugger device to the embedded unit.
3. Start *J-Link GDB Server*.
4. Enter the following values in the configuration dialog:
    - *Connection to J-Link:* USB
    - *Target device:* STM32F302RE
    - *Target interface:* SWD
    
    Click OK. The application should connect to the embedded unit.

![](images/gdb_server_config.png)

Set up a debug configuration in Workbench CANdev:
    
5. Build the projects in your workspace.
6. Right-click on the project you want to start in the debugger and select *Debug As > Hardware Debugging*.
7. Enter the following information in the *Debug Configurations* dialog:
    - *Debugger* tab > *Debugger Type*, set to *JLink*.
    - *Startup* tab > *Initialization Commands*, add `monitor reset 0`.
8. Select *Debug*.

A download dialog should open, then the program should start and stop at a breakpoint in the `main` function.

![](images/debug_configurations_dialog.png)


### How to Read Debug Output Using a Serial Link

The CANdev embedded units write standard output to a serial port. This output can be read, most importantly for debugging purposes.

To do this a USB-to-serial adaptor cable is used. The CPU cards of the embedded units have a PicoBlade 4-pin male socket for its serial port. A serial cable with this female housing is used to connect to it.

The USB-to-serial cable can be purchased for example from [Distrelec](https://www.distrelec.biz/en/picoblade-receptacle-housing-poles-rows-25mm-pitch-molex-51021-0400/p/14354387), the connection housing can be purchased for example from [FTDI](https://ftdi-uk.shop/collections/usb-cables-ttl/products/ttl-232r-3v3-we).

The [PuTTY software](https://www.putty.org/) is used to read from the serial port in this guide.

1. Connect the USB-to-serial cable to the PC and the embedded units.

2. Find out the serial port number: Open Device Manager in Windows, navigate to *Ports (COM & LPT)*, find the COM port name next to the USB-to-serial connection device.

3. Open PuTTY, navigate to *Serial*, enter the following information in the GUI:
    - *Serial line to connect to*: The COM port name, as reported by Device Manager.
    - *Speed*: 115200
    - *Data bits*: 8
    - *Stop bits*: 1
    - *Parity*: None
    - *Flow control*: XON/XOFF.

4. Click *Open*.

A terminal window should open displaying the output from the embedded unit. 

![](images/putty_serial.png)

---

Trouble-Shooting
----------------

### An error message is shown when creating *CANdev Firmware Download* launch configuration

This is most likely caused by missing CAN drivers. To solve this, install the CAN drivers as described in section [*Install CAN Drivers and Related Software*](#install-drivers).

### Unresolved include files in C projects

Users who have had a previous version of Workbench CANdev installed might run into a problem with unresolved included files in their C projects. 

A probable cause of for this is that files for a previous installation lingers in the Windows VirualStore folder.

To solve the problem, delete the installation folder from the `VirtualStore` directory prior to the installation:

    <USER>\AppData\Local\VirtualStore\Program Files (x86)\Kvaser\Workbench...

---

![](images/kvaser_name_logo.png)

[leaf]: https://www.kvaser.com/product/kvaser-leaf-light-hs-v2/
