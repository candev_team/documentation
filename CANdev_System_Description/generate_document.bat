@echo off

REM
REM Pandoc command for generating a PDF file.
REM
REM Pandoc and wkhtmltopdf must be installed.
REM
REM From modified HTML to PDF:
REM pandoc Kingdom_Founder_functionality_description_1.1.html -t html5 --standalone --css pandoc.css --css numbered_sections.css --output Kingdom_Founder_functionality_description_1.1.pdf
REM

@echo on

pandoc CANdev_system_description.md --to html5^
 --metadata title="CANdev System Description"^
 --table-of-contents --standalone^
 --css ../style/documentation_style.css^
 --css ../style/numbered_sections.css^
 --output CANdev_System_Description.html


