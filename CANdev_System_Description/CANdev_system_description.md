<!--
CANdev System Description
=========================
-->

Introduction
------------

The purpose of the document is to be a technical description of the software of the CANdev system, how it is implemented and how it is managed. 

This document should be useful for people who want to familiarise themselves with the implementation and structure of the system. 

The following topics are addressed for each software component:

* General description and purpose of the component.
* How it is packaged in the release package.

The following related information is available for each component in a read-me file in its root directory:
 
* How it is built.
* Which dependencies it has, for building it and for running it.


Structure
---------

The system is stored in a Subversion repository. The repository is located at the following URL at the rt-labs intra network:

http://kvaser.rt-labs.intra/svn/prj_kvaser/lab-kit/

The following figure shows the main directory structure where the software components are stored:

    modules
    ├── bootloader
    ├── bsp
    │   └── boot
    │       └── lab-kit 
    ├── can_kingdom
    ├── candev_examples
    ├── can-loader
    ├── com.kvaser.candevloader
    ├── com.kvaser.kingdomfounder
    ├── docs
    ├── release_package
    └── rt-kernel


Components
----------

This section contains summery descriptions of the various CANdev components.

### Bootloader

The bootloader is a software module which runs on CANdev embedded units. It is responsible for starting the unit, downloading a software application over the network, and running installed application software.

The bootloader should be pre-installed on all CANdev devices that are delivered to end users. It is installed on devices using a J-Link debugger.

----------------

### CAN Kingdom

This is the implementation of the CAN Kingdom stack. It is used as a library by applications that should support CAN Kingdom functionality, such as reacting to King's Document messages.

The CAN Kingdom stack is stored in its own directory in the CANdev release package. It is an Eclipse source code project, which should be imported into an Eclipse workspace to be used. 

-----

### CANdev Examples

The CANdev examples are meant to demonstrate the CANdev system, CAN Kingdom and Kingdom Founder.

They contain Eclipse C projects for embedded units and Kingdom Founder models of systems. The are build by importing them into the CANdev Workbench and issuing the *Build All* command.

The C projects use rt-kernel and the CAN Kingdom stack.

They are stored in the `examples` directory in the release package.

--------

### CAN Loader

The CAN Loader is software written in C that runs at a PC computer to send files to embedded units over CAN. To do this a CAN bridge device must be used. Most importantly, it is used to send executable application files and configuration files.

The CAN Loader is built in two variants:
- A executable terminal program
- A library DLL file. This file is used by the CANdev Loader Eclipse plug-in to use the CAN Loader functionality.

The loader.exe program and its manual are stored in their own directory in the release package.

---------

### Kingdom Founder

Kingdom Founder is an Eclipse based application for creating models for CAN Kingdom networks. It is divided into a number of plug-in projects.

Kingdom Founder is installed by default in the CANdev Workbench.

It is also available in the following forms in the release packages:

* As an update site, which can be used to install it as a plug-in in any Eclipse application.
* As a separate stand-alone application.

-----

### CANdev Loader Plug-in

This is an Eclipse plug-in that calls the CAN Loader to write executable files to embedded devices from within Eclipse.

It is installed by default in the CANdev Workbench, but it can also be installed in any Eclipse application.

-----

### Workbench CANdev

TODO: The workbench project directory is right now located in an internal rt-labs server. It should be moved into the Lab-kit project directory.

TODO: The workbench target platform definition uses an internal rt-labs server for dependencies. It should be changed to instead use public repositories for dependencies.

Workbench CANdev is an Eclipse application that builds on the Eclipse C tools, with extensions for working with rt-kernel projects and CAN Kingdom. 

The base of the workbench consists of the rt-collab workbench, with Kvaser branding.

The workbench code is licensed by rt-labs to Kvaser for used with the CANdev lab-kit only. Any changes in how the workbench is used must be done after the approval of rt-labs.

Two additional plug-ins are installed in rt-collab workbench:
* Kingdom Founder
* CANdev Loader

The workbench application is packaged into the workbench installer. The built workbench zip file must be copied to the `rt-collab-toolbox\workbench` directory before the installer is built.

#### Branding

The branding is the visual appearance of the workbench application. This term references the icons, splash screen and about box of the application.

The main branding of the workbench application is done by modifying the configuration files in these directories:

* `com.rtlabs.workbench.releng.product`
* `com.rtlabs.workbench.product`

The icons of editors and files in the Kingdom Founder application is set in the editor plug-ins:
* `com.kvaser.kingdomfounder.XXXeditor.ui/icons`

It is best to use an Eclipse installation with the Plug-in Developer tools to make changes to branding.

##### Eclipse Product Configuration

The following file is were most of the branding configuration can be done:
* `com.rtlabs.workbench.releng.product/workbench.product`

That file can be opened in the *Product Configuration Editor* in Eclipse. There are tabs in at the bottom of the editor for making changes to the following things:

* exe-file name and icon
* Main window icon
* Startup splash screen
* About box image and text

When any information in the editor has been updated a *Synchronize* command must be issued. This is done from the *Overview* page of the editor.

Read more about *Product Configuration Editor* here:

https://help.eclipse.org/photon/index.jsp?topic=%2Forg.eclipse.pde.doc.user%2Fguide%2Ftools%2Feditors%2Fproduct_editor%2Feditor.htm

#### Building

Use the following command to download dependencies, build and run tests:

    mvn verify

If the build is successful the result can be found in the following sub-directory:

    com.rtlabs.workbench.releng.product\target\products\workbench-XXX.zip

### Dependencies

* A Java Development Kit (JDK) and the Maven software must be installed on the build computer.

--------

### BSP:s

The board support packages (BSP:s) is software that implements support for some particular type of hardware. Embedded application projects link to the BSP:s to setup and interact with the hardware.

The BSP:s source code is located in the `rt-kernel/bsp/` directory, and they are built together with the rt-kernel project.

The BSP:s are packaged together with rt-kernel into the CANdev release package.

In the CANdev Workbench the BSP modules are accessed by importing them into the current workspace using a specialised *BSP Import* Eclipse command.

-------

### rt-kernel

rt-kernel is the real-time embedded operation system that is used in the CANdev embedded units.

It is packaged in the release package and included in new projects that are created with the CANdev Workbench. 

The rt-kernel project includes the BSP:s in the `bsp` directory. 

--------

### Release Package

TODO: The rt-collab-toolbox in located in an internal rt-labs server. It should be copied to the Lab-kit project directory.

The release package directory is used to assemble a CANdev release package from the various components in the project.

The directory contains Subversion links to the components that are included in a release.

A Python script is used to run build steps and extract data into a target directory. The final step in the packaging procedure is to make a zip archive file of the target directory.

#### Dependencies

* A Unix environment (or Cygwin), with a Bash shell. The following software must be installed:
    - Python 2.7
