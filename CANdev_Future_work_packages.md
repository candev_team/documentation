---
author: Jens Lideström
rev: B
date: 2018-10-08
original date: 2018-01-12
---

# Förslag på arbetspaket i CANdev-projektet

## Sammanfattning av förslag

Det följande är några olika arbetspaket som jag skulle kunna ta tag i. 

1. **Förbättra dokumentationen**
    
    Jag tycker den största bristen i nuläget är att det saknas viss dokumentation. All information som behövs för att komma igång och använda CANdev borde finnas i en lättillgänglig form i release-paketet, men det finns hål i täckningen här. Detta skulle jag kunna börja jobba med att fylla igen.

2. **Lägga till ny funktionalitet i CAN Kingdom-stacken**
    
    Framförallt skulle detta innebära att lägga till stöd för King's Page 17-18, dvs att skapa dokument genom att skicka kungsbrev till enheterna.

    Jag har ju bara arbetat med Java i projektet hittills och är ingen embeddedprogrammerare, men jag har erfarenhet av att arbeta med C så jag skulle kunna göra detta.

3. **Lägga till ny funktionalitet i Kingdom Founder**

## Detaljer om förslag

### Dokumentation

1. Dokumentation för hårdvaran:
    - Lista vilka kort som finns, deras syfte och hur dom ska användas.
    - Lista vilka sensorer som finns och hur dom används
    - Beskriv hur BSP:erna funkar och hur dom hänger ihop med hårdvaran. Kanske göra nån lättillgänglig vägledning för BSP:erna.

2. Skapa ett väglednings-dokument för att göra ett enkelt program.
    - En utökad variant av det som finns i Getting started-dokumentet
    - Tex: En enhet som använder en sensor och styr ett servo.
    - Beskriv hela utvecklingsprocessen:
        - Skapa ett projekt
        - Implementera mjukvara
        - Koppla ihop hårdvaran
        - Ladda på mjukvaran på hårdvaran
        - Se resultatet: Servon rör sig och sensorer påverkar.

3. Mer Aircan-dokumentation, vägledning hela vägen tills mjukvaran körs på hårdvaran.

Ungefärlig tidsåtgång: 4 veckor. (Beror på såklart, man kan göra både små och stora insatser.)

### CAN Kingdom-stacken

Utöka och förbättra CAN Kingdom-stacken.

1. Implementera stöd för King's Page 17. (Dvs att skapa dokument genom att skicka King's Page 17 till enheterna.)
2. Allmän översyn och förbättring. Tex införa en del av Bengts förslag.

*Mycket* ungefärlig tidsåtgång för punkt 1: 8 veckor.
Ungefärlig tidsåtgång för punkt 2: 2 veckor. (Beror på såklart, man kan göra både små och stora insatser.)

### Kingdom Founder

Kingdom Founder har ju nu nått ett moget tillstånd. Det finns flera nya saker att lägga till som skulle vara värdefull och användbara, men inga stora hål i funktionaliteten.

1. Importera och exportera system till andra specifikationsformat.
2. Skapa grafisk vy för signalers plats i meddelanden.
3. Lägg till ett sätt att sätta folder-nummer manuellt, för connections och modules.
4. Lägg till integrerad hjälpfunktionalitet, klicka på en hjälp-knapp och öppna relevant avsnitt i dokumentationen.
5. Kopiera saker med Ctrl+C i GUI:t, tex signaler och dokument.
6. Gör så att system-nivå-dokument hänger ihop med city, inte med module.
7. Eventuellt: Hitta på nåt sätt för att undvika duplicering av specifikationen av dokument för sändare och mottagare av meddelanden.

### Blandat

1. Integrera CAN Kingdom-stacken i Workbenchen på ett smidigare sätt. I nuläget får man importera den som ett separat projekt. Den borde ligga på någon intern plats, och länkas till. På samma sätt som rt-kernel.

2. Aircan
    - Förenkla genom att ta bort vissa projekt.
    - Kolla på dom synpunkter som Bengt har framfört.
 
3. Utreda och åtgärda vissa av dom sakerna som Bengt hade synpunkter på om Aircan och BSP:erna
    - Behövs BSP:er som egna projekt?
    - Kan BSP:erna förenklas och göras lättare att förstå för nybörjare?
